
FROM eclipse-temurin:17-jdk-jammy

ARG srcDir=src
WORKDIR /ds
COPY src ./src
COPY target ./src
COPY run.sh /ds
RUN chmod 666 run.sh
RUN run.sh
EXPOSE 8080
